<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Galery;
use File;


class GaleryController extends Controller
{
    
  

    public function index()
    {
        $galery = galery::all();
        return view('galery.index', compact('galery'));
    }

  
    public function create()
    {
        $galery = galery::all();
        return view('galery.create', compact('galery'));
    }

    
    public function store(Request $request)
    {
        $this->validate($request,[
            'image' => 'required|mimes:jpeg,png,jpg '
    	]);
 
        galery::create([
            'image' => $request->image
    	]);
 
    	$gambar = $request->image;
        $name_img = time(). ' - '. $gambar->getClientOriginalName();

        galery::create([
            'image' => $name_img
    	]);

        $gambar->move('img', $name_img);
        return redirect('/galery');
    }


    public function show($id)
    {
        $galery = galery::find($id);
        return view('galery.show', compact('galery'));
    }

    public function edit($id)
    {
        $galery = galery::find($id);
        return view('galery.edit', compact('galery'));
    }

    
    public function update(Request $request, $id)
    {
        $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg',
        ]);

        $galery = galery::findorfail($id);

        if(File::exists(public_path('img/'.$galery->image))){
            File::delete(public_path('img/'.$galery->image));
             
        }
            $gambar = $request->image;
            $new_gambar = time().'-' .$gambar->getClientOriginalName();
            $gambar->move('img', $new_gambar);
            $galery->image = $new_gambar;
            $galery->save();
        return redirect('/galery');
    }

  
    public function destroy($id)
    {
        $galery = galery::find($id);
        $galery->delete();
        return redirect('/galery');
    }

    public function foto()
        {
            $galery = galery::all();
            $data = array('galery'=>$galery);
            return view('web',$data);
        }

}
