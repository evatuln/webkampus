<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Fakultas;
use Auth;

class FakultasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $fakultas = Fakultas::all();
        return view('fakultas.create', compact('fakultas'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_fakultas' => 'required',
            'deskripsi' => 'required',
        ]);
       
        $reqFakultas = $request->all();
        Fakultas::create($reqFakultas);
        //dd($profile);
        return redirect('/fakultas')->with('success', 'Fakultas Keluar Berhasil Disimpan!');
    }


    public function index()
    {
        $fakultas = Fakultas::all();
        return view('fakultas.index', compact('fakultas'));
    }

    public function show($id)
    {
        $fakultas = Fakultas::find($id);
        return view('fakultas.show', compact('fakultas'));
    }

    public function edit($id)
        {
            $fakultas = Fakultas::find($id);
            return view('fakultas.edit', compact('fakultas'));
        }

        public function update($id, Request $request)
            {
                $request->validate([
                    'nama_fakultas' => 'required',
                    'deskripsi' => 'required',
                ]);

                $update = Fakultas::where('id', $id)->update([
                    "nama_fakultas" => $request["nama_fakultas"],
                    "deskripsi" => $request["deskripsi"],
                ]);

                return redirect('fakultas')->with('success', 'Berhasil Update Fakultas!');
            }

            
        public function destroy($id)
        {
            Fakultas::destroy($id);
            return redirect('/fakultas')->with('success', 'Berhasil Update dihapus!');
        }
}
