<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Jurusan;


class JurusanController extends Controller
{
    
   
    public function create()
    {
        $jurusan = Jurusan::all();
        return view('jurusan.create', compact('jurusan'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_jurusan' => 'required',
            'desktripsi' => 'required',
        ]);
       
        $reqJurusan = $request->all();
        Jurusan::create($reqJurusan);
        //dd($profile);
        return redirect('/jurusan')->with('success', 'Jurusan Keluar Berhasil Disimpan!');
    }


    public function index()
    {
        $jurusan = Jurusan::all();
        return view('jurusan.index', compact('jurusan'));
    }

    public function show($id)
    {
        $jurusan = Jurusan::find($id);
        return view('jurusan.show', compact('jurusan'));
    }

    public function edit($id)
        {
            $jurusan = Jurusan::find($id);
            return view('jurusan.edit', compact('jurusan'));
        }

        public function update($id, Request $request)
            {
                $request->validate([
                    'nama_jurusan' => 'required',
                    'deskripsi' => 'required',
                ]);

                $update = Jurusan::where('id', $id)->update([
                    "nama_jurusan" => $request["nama_jurusan"],
                    "deskripsi" => $request["deskripsi"],
                ]);

                return redirect('jurusan')->with('success', 'Berhasil Update Jurusan!');
            }

            
        public function destroy($id)
        {
            Jurusan::destroy($id);
            return redirect('/jurusan')->with('success', 'Berhasil Update dihapus!');
        }

        public function tampilan()
    {
        $jurusan = jurusan::all();
        $data = array('jurusan'=>$jurusan);
        return view('web.index', $data);
    }
}
