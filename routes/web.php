<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GaleryController@foto');


Route::resource('jurusan', 'JurusanController');
Route::resource('fakultas', 'FakultasController');

Route::resource('galery', 'GaleryController');
Auth::routes();


// Route::get('/', function () {
//     return view('Web');
// });



