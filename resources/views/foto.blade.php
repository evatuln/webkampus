<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Biodata</title>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin" />
    <link rel="preload" as="style"
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&amp;family=Roboto:wght@300;400;500;700&amp;display=swap" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&amp;family=Roboto:wght@300;400;500;700&amp;display=swap"
        media="print" onload="this.media='all'" />
    <noscript>
        <link rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&amp;family=Roboto:wght@300;400;500;700&amp;display=swap" />
    </noscript>
    <link href="css/font-awesome/css/all.min.css?ver=1.2.1" rel="stylesheet">
    <link href="css/mdb.min.css?ver=1.2.1" rel="stylesheet">
    <link href="{{ asset('/home/css/aos.css?ver=1.2.1') }}" rel="stylesheet">
    <link href="css/main.css?ver=1.2.1" rel="stylesheet">
    <noscript>
        <style type="text/css">
            [data-aos] {
                opacity: 1 !important;
                transform: translate(0) scale(1) !important;
            }

        </style>
    </noscript>
</head>

<body>
    <div class="flex-center position-ref full-height ">

        <body class="bg-light" id="top">
            <header class="d-print-none">
                <div class="container text-center text-lg-left">
                    <div class="pt-4 clearfix">
                        <h1 class="site-title mb-0"></h1>
                        <div class="site-nav">
                            <nav role="navigation">
                                <ul class="nav justify-content-center">
                                    @if (Route::has('login'))
                                        <div class="top-right links">
                                            @auth
                                                <a href="{{ url('/biodata') }}">CRUD</a>
                                            @else
                                                <a href="{{ route('login') }}">Login</a>

                                                @if (Route::has('register'))
                                                    <a href="{{ route('register') }}">Register</a>
                                                @endif
                                            @endauth
                                        </div>
                                    @endif
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            <div class="page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-8">
                            <div class="card border-warning mb-3">
                                <div class="card-header bg-warning mb-3">
                                    <h2> Galery </h2>
                                </div>

                                @foreach ($galery as $data)
                                    <div class="card" style="width: 7rem;">
                                        <img src="{{ URL::asset('img/' . $data->image) }}" class="card-img-top"
                                            alt="...">
                                    </div>
                                    <div class="mt-2">
                                        <h1> </h1>
                                    </div>

                                @endforeach

                            </div>
                        </div>
                        <div class="col-4">
                            <div class="card border-warning mb-3">
                                <div class="card-body ">
                                    <a class="btn btn-warning" href="/" data-aos="fade-right"
                                        data-aos-delay="700">HOME</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="{{ asset('/home/scripts/mdb.min.js?ver=1.2.1') }}"></script>
            <script src="{{ asset('/home/scripts/aos.js?ver=1.2.1') }}"></script>
            <script src="{{ asset('/home/scripts/main.js?ver=1.2.1') }}"></script>
        </body>

</html>
</div>
</body>

</html>
