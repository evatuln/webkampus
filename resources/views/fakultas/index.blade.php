@extends('layout.master')

@section('title')
    <i class="fas fa-user-tie"></i> Data Fakultas
@endsection

@section('judul')

    <a href="{{ route('fakultas.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i>
        Tambah</a>
@endsection


@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Fakultas</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($fakultas as $key=>$fakultas)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $fakultas->nama_fakultas }}</td>
                    <td>{{ $fakultas->deskripsi }}</td>
                    <td style="display: flex;">
                        <form action="/fakultas/{{ $fakultas->id }}" method="POST">
                            <a href=" {{ route('fakultas.show', ['fakultas' => $fakultas->id]) }}"
                                class="btn btn-info">Show</a>
                            <a href="/fakultas/{{ $fakultas->id }}/edit" class="btn btn-primary">Edit</a>

                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger my-1" cast="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>

@endsection
