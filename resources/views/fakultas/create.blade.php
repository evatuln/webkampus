@extends('layout.master')

@section('title')
    Data Fakultas
@endsection

@section('judul')
    Tambah Data Fakultas
@endsection


@section('content')
    <div>
        <form action="fakultas" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Fakultas</label>
                <input type="text" class="form-control" name="nama_fakultas" id="title"
                    placeholder="Masukkan Nama Fakultas">
                @error('nama_fakultas')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Deskripsi</label>
                <input type="text" class="form-control" name="deskripsi" id="title" placeholder="Masukkan Deskripsi">
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</button>
        </form>
    </div>
@endsection
