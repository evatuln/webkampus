@extends('layout.master')

@section('title')
    Data Fakultas
@endsection

@section('judul')
    Edit Data dengan ID : {{ $fakultas->id }}
@endsection

@section('content')
    <div>
        <form action="/fakultas/{{ $fakultas->id }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama fakultas</label>
                <input type="text" class="form-control" value="{{ $fakultas->nama_fakultas }}" name="nama_fakultas"
                    id="title" placeholder="Masukkan Nama Fakultas">
                @error('nama_fakultas')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Deskripsi</label>
                <input type="text" class="form-control" value="{{ $fakultas->deksripsi }}" name="deksripsi" id="title"
                    placeholder="Masukkan Deskripsi">
                @error('deksripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Simpan Edit</button>
        </form>
    </div>
@endsection
