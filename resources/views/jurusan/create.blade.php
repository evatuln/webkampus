@extends('layout.master')

@section('title')
    Data Jurusan
@endsection

@section('judul')
    Tambah Data Jurusan
@endsection


@section('content')
    <div>
        <form action="jurusan" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Jurusan</label>
                <input type="text" class="form-control" name="nama_jurusan" id="title" placeholder="Masukkan Nama Jurusan">
                @error('nama_jurusan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Deksripsi</label>
                <input type="text" class="form-control" name="deskripsi" id="title" placeholder="Masukkan Nama Deskripsi">
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</button>
        </form>
    </div>
@endsection
