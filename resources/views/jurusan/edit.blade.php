@extends('layout.master')

@section('title')
    Data Jurusan
@endsection

@section('judul')
    Edit Data dengan ID : {{ $jurusan->id }}
@endsection

@section('content')
    <div>
        <form action="/jurusan/{{ $jurusan->id }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama jurusan</label>
                <input type="text" class="form-control" value="{{ $jurusan->nama_jurusan }}" name="nama_jurusan"
                    id="title" placeholder="Masukkan Nama Jurusan">
                @error('nama_jurusan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Deskripsi</label>
                <input type="text" class="form-control" value="{{ $jurusan->deksripsi }}" name="deksripsi" id="title"
                    placeholder="Masukkan Deskripsi">
                @error('deksripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Simpan Edit</button>
        </form>
    </div>
@endsection
