@extends('layout.master')

@section('title')
    <i class="fas fa-user-tie"></i> Data Jurusan
@endsection

@section('judul')

    <a href="{{ route('jurusan.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i>
        Tambah</a>
@endsection


@section('content')

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Jurusan</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($jurusan as $key=>$jurusan)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $jurusan->nama_jurusan }}</td>
                    <td>{{ $jurusan->deskripsi }}</td>
                    <td style="display: flex;">
                        <form action="/jurusan/{{ $jurusan->id }}" method="POST">
                            <a href=" {{ route('jurusan.show', ['jurusan' => $jurusan->id]) }}"
                                class="btn btn-info">Show</a>
                            <a href="/jurusan/{{ $jurusan->id }}/edit" class="btn btn-primary">Edit</a>

                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger my-1" cast="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>

@endsection
