@extends('layout.master')

@section('title')
    <i class="fas fa-user-tie"></i> Data Biodata
@endsection

@section('judul')

    <a href="{{ route('galery.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i>
        Tambah</a>
@endsection

@section('content')
    <div class="ml-3 mt-3 mr-3">
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Foto</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($galery as $key=>$value)
                    <tr>
                        <td>{{ $key + 1 }}</th>
                        <td><img src="{{ asset('img/' . $value->image) }}" height="300px" width="200px"></td>
                        <td>
                            <form action="/galery/{{ $value->id }}" method="POST">
                                <a href="/galery/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>

                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection
@push('scripts')
    <script>
        Swal.fire({
            title: "Berhasil!",
            text: "Masuk ke halaman Galery",
            icon: "success",
            confirmButtonText: "Cool",
        });

    </script>

@endpush
