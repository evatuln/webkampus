@extends('layout.master')

@section('title')
    Galery
@endsection

@section('judul')
    Tambah Data Galery
@endsection
@section('content')
    <div class="ml-3 mr-3">
        <h2>Tambah Data Galery</h2>
        <form action="/galery" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="image">Foto</label>
                <input type="file" class="form-control" name="image" id="image" placeholder="Masukkan Foto">
                @error('image')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
@endsection
