<div class="sidebar">
    <!-- Sidebar user (optional) -->


    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="/biodata" class="nav-link">
                    <i class="nav-icon fas fa-user-tie"></i>
                    <p>
                        Biodata
                    </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="/galery" class="nav-link">
                    <i class="nav-icon far fa-image"></i>
                    <p>
                        Galery
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon fa fa-inbox"></i>
                    <p>
                        Home
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/">
                    <i class=" nav-icon fas fa-sign-out-alt"></i>
                    <p>
                        Sign Out
                    </p>
                   
                </a>
            </li>

    </nav>
    <!-- /.sidebar-menu -->
</div>
